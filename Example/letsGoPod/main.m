//
//  main.m
//  letsGoPod
//
//  Created by Nahar Alkhodair on 07/19/2016.
//  Copyright (c) 2016 Nahar Alkhodair. All rights reserved.
//

@import UIKit;
#import "naharAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([naharAppDelegate class]));
    }
}
