//
//  naharAppDelegate.h
//  letsGoPod
//
//  Created by Nahar Alkhodair on 07/19/2016.
//  Copyright (c) 2016 Nahar Alkhodair. All rights reserved.
//

@import UIKit;

@interface naharAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
