# letsGoPod

[![CI Status](http://img.shields.io/travis/Nahar Alkhodair/letsGoPod.svg?style=flat)](https://travis-ci.org/Nahar Alkhodair/letsGoPod)
[![Version](https://img.shields.io/cocoapods/v/letsGoPod.svg?style=flat)](http://cocoapods.org/pods/letsGoPod)
[![License](https://img.shields.io/cocoapods/l/letsGoPod.svg?style=flat)](http://cocoapods.org/pods/letsGoPod)
[![Platform](https://img.shields.io/cocoapods/p/letsGoPod.svg?style=flat)](http://cocoapods.org/pods/letsGoPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

letsGoPod is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "letsGoPod"
```

## Author

Nahar Alkhodair, nalkhodair@mci.gov.sa

## License

letsGoPod is available under the MIT license. See the LICENSE file for more info.
