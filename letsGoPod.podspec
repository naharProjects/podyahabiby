#
# Be sure to run `pod lib lint letsGoPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#




Pod::Spec.new do |s|
s.name     = 'letsGoPod'
s.version  = '1.0.0'
s.license  = 'MIT'
s.summary  = 'This is a test pod project'
s.homepage = 'https://nakhtop@bitbucket.org/naharProjects/podyahabiby'
s.social_media_url = 'https://twitter.com/nahar_kh'
s.authors  = { 'Nahar Alkhodair' => 'nalkhodair@mci.gov.sa' }
s.source   = { :git => 'https://nakhtop@bitbucket.org/naharProjects/podyahabiby.git', :tag => s.version, :submodules => true }
s.requires_arc = true

# s.public_header_files = 'Pod/Classes/**/*.h'
s.source_files = 'letsGoPod/Classes/**/*'

end


